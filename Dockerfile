FROM scratch
ADD /bin/serve_linux_amd64 /serve
ADD public /public
ENV POLY_HTTP_PORT 80
EXPOSE 80
CMD ["/serve"]
