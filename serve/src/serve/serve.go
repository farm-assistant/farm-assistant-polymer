package main

import (
	"net/http"
	"os"
	"fmt"
)

func main() {
	publicDir := os.Getenv("POLY_PUBLIC_DIR")
	host := os.Getenv("POLY_HTTP_HOST")
	port := os.Getenv("POLY_HTTP_PORT")
	if host == "" {
		fmt.Println("$POLY_HTTP_HOST is required")
		os.Exit(1)
	}
	if port == "" {
		fmt.Println("$POLY_HTTP_PORT is required")
		os.Exit(1)
	}
	if publicDir == "" {
		fmt.Println("$POLY_PUBLIC_DIR is required")
		os.Exit(1)
	}
	http.Handle("/", http.FileServer(http.Dir(publicDir)))
	if err := http.ListenAndServe(host + ":" + port, nil); err != nil {
		fmt.Println(err);
		os.Exit(1);
	}
}
