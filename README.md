# Building

```sh
$ npm run build
```

# Developing

```sh
$ npm run dev

Note: remember to add development: to fass configuration.yaml, and to use the nginx dynamic file (set POLY_STATIC=0 in env.sh) and ensure 
configuration.yaml has
http:
	development:1
```

# Serve

Choice correct `serve` binary for your platform and use it to serve static files 
to frontend server.

```sh
$ ./bin/serve_linux_amd64
```

Requires `POLY_HTTP_HOST` and `POLY_HTTP_PORT` for server binding
and `POLY_PUBLIC_DIR` to find a static files
