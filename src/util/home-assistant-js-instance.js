import HomeAssistant from 'home-assistant-js';

window._hass = new HomeAssistant();
export default window._hass;
