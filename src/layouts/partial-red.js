import hass from '../util/home-assistant-js-instance';

//import INSTANCE from '../../node_modules/home-assistant-js/src/modules/auth/stores/current-auth-store';
import { Store, toImmutable } from 'nuclear-js';


import Polymer from '../polymer';
import nuclearObserver from '../util/bound-nuclear-behavior';


const {
  entityHistoryGetters,
  entityHistoryActions,
  entityGetters,
  configGetters,
  reactor
} = hass;

export default new Polymer({
  is: 'partial-red',
    listeners: {
      'node-red': 'handleNRLoad'
    },
    handleNRLoad: function(data){
      console.log( "NR Load"); 
    },
    handleUriResponse:function(data){
        alert(data);
    },

  behaviors: [nuclearObserver],

  properties: {
    path: {
      type: String,
      value:"",
    },
    narrow: {
      type: Boolean,
      value: false,
    },
    showMenu: {
      type: Boolean,
      value: false,
    },
    frameName:{
        type:String,
        value:'red-frame',
    },
    domainName:{
        type:String,
        value:'nodered',
    },
    uri:{
      type:String,
      value:"/nodered"
    },
    entities: {
      type: Array,
      bindNuclear: [
        entityGetters.entityMap,
        (map) => map.valueSeq().sortBy((entity) => entity.entityId).toArray(),
      ],
    },

  },
  noderedUri(){
    return this.uri;
  },
  prepare(){
    var $this = this;
    setInterval(function(){
            if (window.location.href == $this.path){
                if (document.activeElement.id != $this.frameName){
                    document.querySelector('#' + $this.frameName).focus();
                }
            }
        },100);
    },
  attached() {
    console.log("partial-red Attached");
    var $this = this;
    this.path = window.location.href;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE){
            if (xhr.status == 200) {
                document.querySelector('#' + $this.frameName).src  = $this.uri; // make it dyanmic. Use haproxy to consolidate the port. Parse the hostname
                //console.log(document.querySelector('#' + $this.frameName).src );
               $this.calcHeight();
               $this.prepare();
           } else {
                setTimeout(function(){ $this.attached() },1000);
           }
        }
    }
    var authToken = reactor.evaluate(['authCurrent', 'authToken']);

    var cred = (!authToken) ? "": '?api_password=' + authToken;
    var _url = (!authToken) ? $this.uri : $this.uri + "/login"  + cred + '&rand=' + Math.random().toString() 
    xhr.open("GET", _url , true);
    var resp = xhr.send();
  },
  resizeDone: function(e, detail, sender) {
    this.calcHeight();
  },
  calcHeight() {
   var tbar = document.querySelector('#drawer > ha-sidebar > paper-header-panel > paper-toolbar');
   var frame = document.querySelector('#' + this.frameName);
   var ht =  window.screen.height - tbar.clientHeight;
   frame.height =  (frame.height != ht) ? ht: frame.height;
   //frame.setAttribute('top',tbar.clientHeight); // Doesn't work
  },
  toggleMenu() {
    this.fire('open-menu');
  },
  computeMenuButtonClass(narrow, showMenu) {
    return !narrow && showMenu ? 'invisible' : '';
  },
  computeContentClasses(narrow) {
    return `flex content ${narrow ? 'narrow' : 'wide'}`;
  },
});
