import hass from '../util/home-assistant-js-instance';

import Polymer from '../polymer';
import nuclearObserver from '../util/bound-nuclear-behavior';

require('../components/entity/ha-entity-marker');

const {
  configGetters,
  entityGetters,
} = hass;

window.L.Icon.Default.imagePath = '/static/images/leaflet';

export default new Polymer({
  is: 'partial-map',

  behaviors: [nuclearObserver],

  properties: {
    locationGPS: {
      type: Number,
      bindNuclear: configGetters.locationGPS,
    },

    locationName: {
      type: String,
      bindNuclear: configGetters.locationName,
    },

    tilesUrl: {
      type: String,
      value: "http://tiles-{s}.data-cdn.linz.govt.nz/services;key=63df6500b48f4cb68678c67f0c052ea3/tiles/v4/set=2/EPSG:3857/{z}/{x}/{y}.png",
    },

    locationEntities: {
      type: Array,
      bindNuclear: [
        entityGetters.visibleEntityMap,
        entities => entities.valueSeq().filter(
          (entity) => {
		console.log("Filtering entitiy: ", entity);
		return entity.attributes.latitude;
	}
        ).toArray(),
      ],
    },

    zoneEntities: {
      type: Array,
      bindNuclear: [
        entityGetters.entityMap,
        entities => entities.valueSeq()
          .filter(entity => entity.domain === 'zone' &&
                            !entity.attributes.passive)
          .toArray(),
      ],
    },

    narrow: {
      type: Boolean,
    },

    showMenu: {
      type: Boolean,
      value: false,
    },
  },

  attached() {
    const map = this.$.map;
    var me = this;
    // On iPhone 5, 5s and some 6 I have observed that the user would be
    // unable to pan on initial load. This fixes it. Also on Safari
    if (window.L.Browser.mobileWebkit || window.L.Browser.webkit) {
      this.async(() => {
        const prev = map.style.display;
        map.style.display = 'none';
        this.async(() => { map.style.display = prev; }, 1);
      }, 1);
    }
    // listen on zoom event to adjust icon size
    map.addEventListener('zoomend', function(ev) {
      me.async(() => {
        var iconSize = (map.zoom >= 8 && map.zoom <= 12) ? 0 : (5*map.zoom-50); 
        Array.prototype.forEach.call(document.getElementsByClassName('marker'), function(marker) {
          marker.style.width = `${iconSize}px`;
          marker.style.height = `${iconSize}px`;
        });
      }, 1);
    });
  },

  computeMenuButtonClass(narrow, showMenu) {
    return !narrow && showMenu ? 'invisible' : '';
  },

  toggleMenu() {
    this.fire('open-menu');
  },
});
