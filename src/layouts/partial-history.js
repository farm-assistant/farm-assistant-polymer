import hass from '../util/home-assistant-js-instance';

import Polymer from '../polymer';
import nuclearObserver from '../util/bound-nuclear-behavior';

require('./partial-base');
require('../components/state-history-charts');
require('../components/period-picker');

const {
  entityHistoryGetters,
  entityHistoryActions,
} = hass;

export default new Polymer({
  is: 'partial-history',

  behaviors: [nuclearObserver],

  properties: {
    narrow: {
      type: Boolean,
    },

    showMenu: {
      type: Boolean,
      value: false,
    },

    stateHistory: {
      type: Object,
      bindNuclear: entityHistoryGetters.entityHistoryMap,
    },

    isLoadingData: {
      type: Boolean,
      bindNuclear: entityHistoryGetters.isLoadingEntityHistory,
    },

  },

  toggleMenu() {
    this.fire('open-menu');
  },

  handleRefreshClick() {
    entityHistoryActions.fetchSelectedPeriod();
  },

  computeMenuButtonClass(narrow, showMenu) {
    return !narrow && showMenu ? 'invisible' : '';
  },

  computeContentClasses(narrow) {
    return `flex ${narrow ? 'narrow' : 'wide'}`;
  },
});
