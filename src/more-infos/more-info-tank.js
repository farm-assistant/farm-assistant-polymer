import hass from '../util/home-assistant-js-instance';

import Polymer from '../polymer';

const { serviceActions } = hass;

export default new Polymer({
  is: 'more-info-tank',
  handleSubmit() {
    serviceActions.callService('tank', 'state', {id: this.stateObj.objectId, height: this.value});
  },
  properties: {
    stateObj: {
      type: Object,
      observer: 'stateObjChanged',
    },
    value: {
      type: String,
      value: '',
    },
    valueValid: {
      type: Boolean,
      computed: 'validateHeight(value)',
    },
  },
  validateHeight(code) {
    return true;
  },
  stateObjChanged(newVal) {
    var state = JSON.parse(newVal.state);
    this.stateObj = newVal;
    this.value = state.height || '';
  },
});
