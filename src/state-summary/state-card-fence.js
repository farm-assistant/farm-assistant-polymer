import Polymer from '../polymer';

require('../components/state-info');
require('../components/entity/ha-entity-toggle');

import hass from '../util/home-assistant-js-instance';
const { serviceActions } = hass;

export default new Polymer({
  is: 'state-card-fence',

  properties: {
    stateObj: {
      type: Object,
    },

    toggleChecked: {
      type: Boolean,
      computed: 'computeToggleChecked(stateObj, toggleDisabled)',
    },

    toggleDisabled: {
      type: Boolean,
      computed: 'computeToggleDisabled(stateObj)',
    }
  },
 
  computeToggleChecked: function(stateObj, toggleDisabled) {
    var value = stateObj.state > 0;
    if (toggleDisabled) {
      value = !value;
    }
    console.log('name', stateObj.attributes.friendly_name, 'state', stateObj.state, 'checked', value, 'disabled', toggleDisabled);
    return value;
  },

  computeToggleDisabled: function(stateObj) {
    return stateObj.attributes.is_toggling;
  },

  toggleChanged: function(e) {
    serviceActions.callService('fence', 'state',
      {id: this.stateObj.objectId, toggle: true});
  },
});
