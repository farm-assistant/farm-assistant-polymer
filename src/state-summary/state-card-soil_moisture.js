import Polymer from '../polymer';

export default new Polymer({
  is: 'state-card-soil_moisture',

  properties: {
    stateObj: {
      type: Object,
    },
  },

});
