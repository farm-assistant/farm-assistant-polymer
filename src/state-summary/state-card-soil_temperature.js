import Polymer from '../polymer';

export default new Polymer({
  is: 'state-card-soil_temperature',

  properties: {
    stateObj: {
      type: Object,
    },
  },

});
