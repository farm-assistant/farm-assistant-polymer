import Polymer from '../polymer';

export default new Polymer({
  is: 'camera-slider',

  properties: {
    frames: {
      type: Array,
    },

    sliderValue: {
      type: Number,
      value: 0,
    },

    caption: {
      type: String,
      computed: 'computeCaption(activeFrame)'
    },

    assumedValue: {
      type: Number,
      value: 0,
    },

    activeFrame: {
      type: Object,
      computed: 'computeActiveFrame(frames, sliderValue)',
    },

    sliderLength: {
      type: Number,
      computed: 'computeSliderLength(frames)',
    },
  },


  computeSliderLength(frames) {
    return frames.length && frames.length - 1;
  },

  computeActiveFrame(frames, sliderValue) {
    const frame = frames[sliderValue >= frames.length ? frames.length - 1 : sliderValue];
    return frame;
  },

  computeCaption(frame) {
    console.log(frame);
    return new Date(frame.updated_at * 1000).toLocaleString();
  },

  frameSelected(e) {
    const value = e.target.immediateValue;
    this.assumedValue = e.target.immediateValue;
    // active frame should be changed with delay to prevent intermediate
    // frames from loading
    this.async(() => {
      if (this.assumedValue === value) {
        this.sliderValue = this.assumedValue;
      }
    }, 200);
  },

});
