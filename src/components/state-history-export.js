import Polymer from '../polymer';
import hass from '../util/home-assistant-js-instance';

export default new Polymer({
  is: 'state-history-export',

  properties: {
    data: {
      type: Object,
    },
    
    selectedPeriod: {
      type: String,
      value: null,
    },
  },

  computeLabel: function(item) {
    return `CSV for ${item[0].id}`;
  },

  computeURL: function(item) {
    const start = hass.util.encodeDate(this.selectedPeriod.start);
    const end = hass.util.encodeDate(this.selectedPeriod.end);

    return `/api/history/period?start=${start}&end=${end}.csv?filter_entity_id=${item[0].id}`;
  }
});
