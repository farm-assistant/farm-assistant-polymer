import Polymer from '../polymer';
import hass from '../util/home-assistant-js-instance';

require('./state-history-line');
require('./state-history-camera');

const {
  entityHistoryActions,
} = hass;

export default new Polymer({
  is: 'state-history-chart-content',

  properties: {
    domain: {
      type: String,
    },
    entities: {
      type: Object,
    },

    data: {
      type: Array,
      computed: 'computeData(entities)',
    },

    isLineChart: {
      type: Boolean,
      computed: 'computeIsLineChart(domain)',
    },

    isCamera: {
      type: Boolean,
      computed: 'computeIsCamera(domain)',
    },

  },

  computeIsLineChart(domain) {
    return ['loadcell', 'fence', 'tank', 'soil_temperature', 'soil_moisture']
      .indexOf(domain) !== -1;
  },

  computeIsCamera(domain) {
    return domain === 'camera';
  },

  computeData(entities) {
    // chart restamp hack
    const template = this.$.cards;
    template.if = false;
    setTimeout(() => {
      template.if = true;
    });

    return entities;
  },

});
