import Polymer from '../polymer';

require('./camera-slider');

export default new Polymer({
  is: 'state-history-camera',

  properties: {
    data: {
      type: Array,
      value: [],
    },

    selected: {
      type: Number,
      value: 0,
    },

    frames: {
      type: Object,
      computed: 'computeFrames(filteredData, selected)',
    },

    filteredData: {
      type: Object,
      computed: 'computeFilteredData(data)',
    },
  },

  computeFilteredData(data) {
    return data
      .map((entity, entityId) => ({
        id: entityId,
        key: entity.get('friendly_name') || entityId,
        values: entity.get('states')
          .filter(s => s.get('value') && s.get('value') !== 'unknown')
          .toJS(),
      }))
      .filter(entity => entity.values.length)
      .toList().toJS();
  },

  computeFrames(data, selected) {
    console.log('got', data);
    if (!data.length) {
      return [];
    }
    const camera = data[selected];
    return camera.values
      .map((state, index) => ({
        index,
        url: `/api/camera_history/${camera.id}/${state.value}`,
        updated_at: state.updated_at,
      }));
  },
});
