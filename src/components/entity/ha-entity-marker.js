import Polymer from '../../polymer';
import hass from '../../util/home-assistant-js-instance';
import stateIcon from '../../util/state-icon';

require('../../components/ha-label-badge');

const {
  reactor,
  entityGetters,
  moreInfoActions,
} = hass;

export default new Polymer({
  is: 'ha-entity-marker',

  properties: {
    entityId: {
      type: String,
      value: '',
    },

    state: {
      type: Object,
      computed: 'computeState(entityId)',
    },

    icon: {
      type: Object,
      computed: 'computeIcon(state)',
    },

    image: {
      type: Object,
      computed: 'computeImage(state)',
    },

    value: {
      type: String,
      computed: 'computeValue(state)',
    },

    valueDisplay: {
      type: String,
      computed: 'computeValueDisplay(state)',
    },
  },

  listeners: {
    tap: 'badgeTap',
  },

  badgeTap(ev) {
    ev.stopPropagation();
    if (this.entityId) {
      this.async(() => moreInfoActions.selectEntity(this.entityId), 1);
    }
  },

  computeState(entityId) {
    console.log("Got an entity. id:", entityId);
    return entityId && reactor.evaluate(entityGetters.byId(entityId));
  },

  computeIcon(state) {
    console.log("Finding icon for", state.domain);
    switch (state.domain) {
      case 'loadcell':
        return 'agsense:load-cell';
      case 'relay':
        return stateIcon(state);
      case 'soil_moisture':
        return 'agsense:soil-moisture2';
      case 'soil_temperature':
        return 'agsense:soil-temperature';
      default:
        return !state && 'home';
    }
  },

  computeImage(state) {
    console.log("Finding IMAGE for ", state.domain);
    switch (state.domain) {
      case 'tank':
        return '/static/map-icons/tank.svg';
      case 'fence':
        if (parseInt(state.state) > 0) {
          return '/static/map-icons/fence-active.svg';
        }
        return '/static/map-icons/fence-inactive.svg';
      default:
        return state && state.attributes.entity_picture;
    }
  },

  computeValue(state) {
    console.log(`Creating value for ${state.domain} with ${JSON.stringify(state)}`);
    switch (state.domain) {
      case 'tank':
      case 'relay':
      case 'fence':
      case 'soil-moisture': //return state.state;
        return null;
      default:
        return state && state.entityDisplay.split(' ').map(part => part.substr(0, 1)).join('');
    }
  },

  computeValueDisplay(state) {
    let value;
    switch (state.domain) {
      case 'tank':
        value = Math.round(state.state);
        break;
      default:
        value = state.state;
    }
    return state ?
      `${value}\u00A0${state.attributes.unit_of_measurement || ''}` : null;
  },
});
