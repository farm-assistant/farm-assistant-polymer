import * as d3 from 'd3';
import * as _ from 'lodash';

import Polymer from '../polymer';
import hass from '../util/home-assistant-js-instance';

const {
  encodeDate,
  entityHistoryActions,
} = hass;

const getX = d => d.updated_at;
const getY = d => !isNaN(d.value) ? Number(d.value) : 0;
const getTitle = d => d.key;
const getSeriesClass = z => d => `series-${z(getTitle(d))}`;
const getUnits = d => d.unit_of_measurement;

export default new Polymer({
  is: 'state-history-line',

  behaviors: [
    Polymer.IronResizableBehavior,
  ],

  properties: {
    el: {
      type: Object,
    },

    width: {
      type: Number,
    },

    height: {
      type: Number,
    },

    isInitialized: {
      type: Boolean,
      value: false,
    },

    // chart options

    focusMargin: {
      type: Object,
      value: {top: 40, right: 25, bottom: 100, left: 40},
    },

    contextMargin: {
      type: Object,
      value: {top: 320, right: 20, bottom: 30, left: 40},
    },

    colorScale: {
      type: Object,
      value: () => d3.scaleOrdinal().range(_.range(10)),
    },

    // chart internals

    focusScales: {
      type: Object,
      value: function() {
        return {
          x: d3.scaleTime(),
          y: d3.scaleLinear(),
        }
      }
    },

    focusLine: {
      type: Object,
    },

    focusAxes: {
      type: Object,
    },

    focus: {
      type: Object,
    },

    context: {
      type: Object,
    },

    legend: {
      type: Object,
    },

    contextScales: {
      type: Object,
      value: function() {
        return {
          x: d3.scaleTime(),
          y: d3.scaleLinear(),
        }
      }
    },

    contextAxes: {
      type: Object,
    },

    clipPathRect: {
      type: Object,
    },

    brush: {
      type: Object,
    },

    convertedData: {
      type: Object,
      computed: 'computeConvertedData(data)',
    },

    disabledSeries: {
      type: Object,
      value: {},
    },
  },

  observers: [
    'onFocusResize(width, height, focusMargin)',
    'onContextResize(width, height, contextMargin)',
  ],

  listeners: {
    'iron-resize': 'onResize',
  },

  onResize: function() {
    if (this.el) {
      this.width = this.el.width.baseVal.value;
      this.height = this.el.height.baseVal.value;
    }
  },

  onFocusResize: function(width, height, margin) {
    const focusWidth = width - margin.left - margin.right;
    const focusHeight = height - margin.top - margin.bottom;

    this.focusScales.x.range([0, focusWidth]);
    this.focusScales.y.range([focusHeight, 0]);

    // XXX will it blend?
    this.focusAxes = {
      y: d3.axisLeft(this.focusScales.y),
      x: d3.axisBottom(this.focusScales.x).ticks(7),
    };

    this.clipPathRect.attr("width", focusWidth)
                     .attr("height", focusHeight);

    this.focus.attr("transform", `translate(${margin.left},${margin.top})`);

    this.focus.select('.axis--x')
              .attr("transform", `translate(0,${focusHeight})`)
              .call(this.focusAxes.x);


    this.focus.select('.axis--y')
              .call(this.focusAxes.y);

  },

  onContextResize: function(width, height, margin) {
    const contextWidth = width - margin.left - margin.right;
    const contextHeight = height - margin.top - margin.bottom;

    this.contextScales.x.range([0, contextWidth]);
    this.contextScales.y.range([contextHeight, 0]);

    this.contextAxes = {
      x: d3.axisBottom(this.contextScales.x).ticks(7),
    };

    this.brush.extent([[0, 0], [contextWidth, contextHeight]]);

    this.context.select('.brush').call(this.brush);

    this.context.attr("transform", `translate(${margin.left},${margin.top})`);

    this.context.select('.background')
      .attr('width', contextWidth)
      .attr('height', contextHeight);

    this.context.select('.axis')
                  .attr("transform", `translate(0,${contextHeight})`)
                  .call(this.contextAxes.x);
  },

  initializeChart: function() {
    this.scopeSubtree(this.el, true);

    const svg = d3.select(this.el);

    this.clipPathRect = svg.append("defs").append("clipPath")
      .attr("id", "clip")
      .append("rect");

    this.focus = svg.append("g")
      .attr("class", "focus");

    this.focus.append("g")
      .attr("class", "axis axis--x");

    this.focus.append("g")
      .attr("class", "axis axis--y");

    this.context = svg.append("g")
      .attr("class", "context");

    this.context.append("g")
      .attr("class", "axis axis--x");

    this.brush = d3.brushX()
      .on("brush end", this.onBrushed.bind(this));

    const brushG = this.context.append("g")
      .attr('class', 'brush');

    brushG.append('rect')
      .attr('class', 'background');

    brushG.append('g')
      .attr('class', 'lines');

    brushG.append('rect')
      .attr('class', 'overlay');

    this.legend = svg.append("g")
      .attr("class", "legend")
      .attr("transform", "translate(40, 0)");

    this.focusLine = d3.line()
      .curve(d3.curveMonotoneX)
      .defined(function(d) { return !isNaN(getY(d)); })
      .x(d => this.focusScales.x(getX(d)))
      .y(d => this.focusScales.y(getY(d)));

    this.isInitialized = true;
  },

  redrawFocusLines: function() {
    const data = this.convertedData;
    var entity = this.focus.selectAll('.entity')
      .data(data)
      .classed('disabled', d => this.disabledSeries[d.key])
      .enter()
      .append('g')
      .attr("class", "entity")


      entity.append("path")
      .attr("class", d => `line ${getSeriesClass(this.colorScale)(d)}`)
      .attr("d", d => this.focusLine(d.states));

    entity.exit().remove()
  },

  onBrushed: function() {
    var s = d3.event.selection || this.contextScales.x.range();
    this.focusScales.x.domain(s.map(this.contextScales.x.invert, this.contextScales.x));
    this.focus.selectAll(".line").attr("d", d => this.focusLine(d.states));
  },

  computeConvertedData(data) {
    const selectedPeriod = entityHistoryActions.getSelectedPeriod();
    return data.map((entity, entityId) => {
      const states = entity.get('states').map(state => ({
        updated_at: new Date(state.get('updated_at') * 1000),
        value: state.get('value'),
      })).toJS();
      states.unshift({ updated_at: selectedPeriod.start, value: NaN });
      states.push({ updated_at: selectedPeriod.end, value: NaN });
      return {
        key: entity.get('friendly_name') || entityId,
        units_of_measurement: entity.get('unit_of_measurement'),
        states,
      };
    }).toList().toJS();
  },

  onDataUpdate() {
    const data = this.convertedData;
    // legend
    const wrap = this.legend.selectAll('.entity')
      .data(data);

    let legendOffset = 0;
    const gEnter = wrap.classed('disabled', d => d.disabled)
      .enter().append('g')
      .attr('class', 'entity')
      .append('g')
      .attr('transform', (d) => {
        const labelWidth = (d.key.length - 1) * 8;
        const labelOffset = legendOffset;
        legendOffset += labelWidth + 20;
        return `translate(${labelOffset},20)`;
      });

    gEnter.append('circle')
      .style('stroke-width', 2)
      .attr('r', 5)
      .attr('class', getSeriesClass(this.colorScale));

    gEnter.append('text')
      .attr('dy', '.4em')
      .attr('dx', '8')
      .text(getTitle);

    gEnter.on('click', (d) => {
      this.disabledSeries[d.key] = !this.disabledSeries[d.key];
      this.onDataUpdate();
    });

    // focus

    this.focusScales.x.domain([
      d3.min(data, e => d3.min(e.states, getX)),
      d3.max(data, e => d3.max(e.states, getX)),
    ]);


    this.focusScales.y.domain([
      d3.min(data, e => (getUnits(e) === '%' ? 0 : d3.min(e.states, getY))),
      d3.max(data, e => (getUnits(e) === '%' ? 100 : d3.max(e.states, getY))),
    ]);

    this.colorScale.domain(data.map(getTitle));

    this.redrawFocusLines();

    // context

    this.contextScales.x.domain(this.focusScales.x.domain());
    this.contextScales.y.domain(this.focusScales.y.domain());

    var entity = this.context.select('.brush .lines').selectAll('.entity')
      .data(data)
      .classed('disabled', d => this.disabledSeries[d.key])
      .enter()
      .append('g')
      .attr("class", "entity");

    var contextLine = d3.line()
      .curve(d3.curveMonotoneX)
      .defined(function(d) { return !isNaN(getY(d)); })
      .x(d => this.contextScales.x(getX(d)))
      .y(d => this.contextScales.y(getY(d)));

    entity.append("path")
      .attr("class", d => `line ${getSeriesClass(this.colorScale)(d)}`)
      .attr('d', d => contextLine(d.states));

    entity.exit().remove();

    this.focus.select('.axis--x').call(this.focusAxes.x);
    this.focus.select('.axis--y').call(this.focusAxes.y);
    this.context.select('.axis--x').call(this.contextAxes.x);
  },

  exportMenuItems(data) {
    const selectedPeriod = entityHistoryActions.getSelectedPeriod();
    const start = encodeDate(selectedPeriod.start);
    const end = encodeDate(selectedPeriod.end);
    return data.map((entity, entityId) => ({
      url: `/api/history/period.csv?start=${start}&end=${end}
           &filter_entity_id=${entityId}`,
      title: entity.get('friendly_name') || entityId,
    })).toArray();
  },

  attached() {
    this.el = this.$$('svg');
    this.initializeChart();

    this.async(this.notifyResize, 1);
    this.async(this.onDataUpdate, 1);
  },
});
