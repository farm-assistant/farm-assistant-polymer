import Polymer from '../polymer';

require('./loading-box');
require('./state-history-chart-content');
require('./period-picker');

export default new Polymer({
  is: 'state-history-charts',

  properties: {
    stateHistory: {
      type: Object,
    },

    isLoadingData: {
      type: Boolean,
      value: false,
    },

    // ui

    cards: {
      type: Object,
      computed: 'computeCards(stateHistory, isLoadingData)',
    },

  },

  computeCards(stateHistory, isLoadingData) {
    const cards = {
      columns: [],
    };

    if (isLoadingData) {
      return cards;
    }

    const columns = 2;

    for (let idx = 0; idx < columns; idx++) { cards.columns[idx] = []; };

    let index = 0;
    function increaseIndex() {
      const old = index;
      index = (index + 1) % columns;
      return old;
    }

    const supportedDomains = [
      'loadcell',
      'tank', 'camera', 'soil_temperature', 'soil_moisture'];

    stateHistory.filter(entity => entity.get('states').size > 0)
        .groupBy((_, entityId) => entityId.split('.')[0])
        .filter((_, domain) => supportedDomains.indexOf(domain) !== -1)
        .forEach((entities, domain) => {
          cards.columns[increaseIndex()].push({ domain, entities });
        });

    return cards;
  },

  computeIsSingleDevice(stateHistory) {
    return stateHistory && stateHistory.size === 1;
  },

  computeContentClasses(isLoadingData) {
    return isLoadingData ? 'loading' : '';
  },
});
