import Polymer from '../polymer';

import hass from '../util/home-assistant-js-instance';
import nuclearObserver from '../util/bound-nuclear-behavior';

const {
  entityHistoryGetters,
  entityHistoryActions,
} = hass;

export default new Polymer({
  is: 'period-picker',

  properties: {
    selectedPeriod: {
      type: Object,
      observer: 'selectedPeriodChanged',
    },

    selectedPeriodLabel: {
      type: String,
      computed: 'computeSelectedPeriodLabel(selectedPeriod)',
    },

    periods: {
      type: Array,
      value: [
        {
          title: 'Today', callback: () => {
            const end = new Date();
            const start = new Date();
            start.setHours(0); start.setMinutes(0); start.setSeconds(0);
            end.setHours(23); end.setMinutes(59); start.setSeconds(59);
            return { start, end };
          },
        },
        {
          title: 'Last week', callback: () => {
            const end = new Date();
            const start = new Date();
            start.setHours(0); start.setMinutes(0); start.setSeconds(0);
            start.setDate(start.getDate() - 7);
            return { start, end };
          },
        },
        {
          title: 'Last month', callback: () => {
            const end = new Date();
            const start = new Date();
            start.setHours(0); start.setMinutes(0); start.setSeconds(0);
            start.setDate(start.getDate() - 30);
            return { start, end };
          },
        },
        {
          title: 'Last year', callback: () => {
            const end = new Date();
            const start = new Date();
            start.setHours(0); start.setMinutes(0); start.setSeconds(0);
            start.setDate(start.getDate() - 365);
            return { start, end };
          },
        }
      ],
    },
  },

  computeSelectedPeriodLabel(p) {
    if (p) {
      let label;
      if (p.start.getFullYear() === p.end.getFullYear() &&
        p.start.getMonth() === p.end.getMonth() &&
          p.start.getDate() === p.end.getDate()) {
            label = p.start.toDateString();
          } else {
            label = `${p.start.toDateString()} to ${p.end.toDateString()}`;
          }
      return label;
    }
  },

  togglePicker() {
    this.$.dropdown.toggle();
  },

  periodSelected(ev) {
    const title = ev.target.textContent.trim();
    this.periods.find(p => {
      if (p.title === title) {
        entityHistoryActions.selectPeriod(p.callback());
        this.selectedPeriod = entityHistoryActions.getSelectedPeriod();
        this.togglePicker();
        return true;
      }
    });
  },

  periodEndChanged(ev, args) {
    entityHistoryActions.selectPeriod({
      start: this.selectedPeriod.start,
      end: args.date});
    this.selectedPeriod = entityHistoryActions.getSelectedPeriod();
  },

  periodStartChanged(env, args) {
    entityHistoryActions.selectPeriod({
      start: args.date,
      end: this.selectedPeriod.end});
    this.selectedPeriod = entityHistoryActions.getSelectedPeriod();
  },

  selectedPeriodChanged(period) {
    console.log('CHANGED', period);
    entityHistoryActions.fetchPeriod(period);
  },

  attached() {
    // definitely there is magic involved with reactor behavior
    // setting value manually on component init
    if (this.selectedPicker === undefined) {
      entityHistoryActions.selectPeriod(this.periods[0].callback());
    }
    this.selectedPeriod = entityHistoryActions.getSelectedPeriod();
  }

});
