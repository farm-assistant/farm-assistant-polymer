var path = require('path');
var webpack = require("webpack");

const DEV = JSON.stringify(JSON.parse(process.env.POLY_DEV || 'true'));

var definePlugin = new webpack.DefinePlugin({
  __DEV__: DEV,
  __DEMO__: JSON.stringify(JSON.parse(process.env.BUILD_DEMO || 'false')),
});

module.exports = {
  cache: true,
  entry: {
    app: './src/home-assistant.js',
    service_worker: './src/service-worker/index.js',
  },
  output: {
    path: path.join(__dirname, 'build'),
    publicPath: "/public",
    filename: 'static/[name].js',
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        test: /.js$/,
        include: [
          path.resolve(__dirname, 'src'),
          path.resolve(__dirname, 'node_modules/home-assistant-js/src'),
        ],
      },
    ],
  },
  plugins: [
    definePlugin,
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /no-other-locales-for-now/),
  ],
};
